/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * a reference to another template resource
 *
 * @author Nino
 */
public class Ref {

    @JsonProperty("Ref")
    private String name;

    public Ref(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
