/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author VIDOA
 */
public class Resource {

    @JsonIgnore
    public String name;

    @JsonIgnore
    public Ref getRef() {
        return new Ref(name);
    }

}
