/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen.iam;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.vontobel.access.info.cf.template.gen.Ref;
import com.vontobel.access.info.cf.template.gen.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author VIDOA
 */
public class IAMPolicy extends Resource {

    @JsonProperty("Type")
    public final String TYPE = "AWS::IAM::Policy";

    @JsonProperty("Properties")
    Properties properties = new Properties();

    public IAMPolicy(String name) {
        this.name = name;
    }

    public void addPolicyDocument(String document) {
        this.properties.policy = document;
    }

    public void addRoleRef(Ref role) {
        this.properties.roles.add(role);
    }

    public void addPolicyName(String name) {
        this.properties.policyName = name;
    }

    public static class Properties {

        @JsonRawValue
        @JsonProperty("PolicyDocument")
        public String policy;

        @JsonProperty("PolicyName")
        public String policyName;

        @JsonProperty("Roles")
        List<Ref> roles = new ArrayList<>();
    }

}
