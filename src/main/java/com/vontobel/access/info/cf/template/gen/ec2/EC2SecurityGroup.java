/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen.ec2;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vontobel.access.info.cf.template.gen.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author VIDOA
 */
public class EC2SecurityGroup extends Resource {

    public static final String TCP_PROTOCOL = "tcp";

    @JsonProperty("Type")
    public final String TYPE = "AWS::EC2::SecurityGroup";

    @JsonProperty("Properties")
    public Properties properties = new Properties();

    public EC2SecurityGroup(String name, String description) {
        this.name = name;
        properties.groupDescription = description;
    }

    public void addIngressRule(EC2SecRule rule) {
        properties.securityGroupIngress.add(rule);
    }

    public static class Properties {

        @JsonProperty("GroupDescription")
        String groupDescription;

        @JsonProperty("SecurityGroupIngress")
        List<EC2SecRule> securityGroupIngress = new ArrayList<>();

    }

    public static class EC2SecRule {

        public EC2SecRule(String cidrIp, String fromPort, String ipProtocol, String toPort) {
            this.cidrIp = cidrIp;
            this.fromPort = fromPort;
            this.ipProtocol = ipProtocol;
            this.toPort = toPort;
        }

        @JsonProperty("CidrIp")
        String cidrIp;
        @JsonProperty("FromPort")
        String fromPort;
        @JsonProperty("IpProtocol")
        String ipProtocol;
        @JsonProperty("ToPort")
        String toPort;

    }
}
