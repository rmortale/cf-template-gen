/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen.ec2;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.vontobel.access.info.cf.template.gen.Ref;
import com.vontobel.access.info.cf.template.gen.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author VIDOA
 */
public class EC2Instance extends Resource {

    @JsonProperty("Type")
    public final String TYPE = "AWS::EC2::Instance";

    @JsonProperty("Properties")
    EC2Properties properties = new EC2Properties();

    public EC2Instance() {

    }

    public EC2Instance(String name, String imageId, String instanceType) {
        this.name = name;
        properties.imageId = imageId;
        properties.instanceType = instanceType;
    }

    public void addInstanceProfileRef(Ref ref) {
        this.properties.iamInstanceProfile = ref;
    }

    public void addKeyNameRef(Ref ref) {
        this.properties.keyName = ref;
    }

    public void addSecurityGroupRef(Ref ref) {
        this.properties.securityGroups.add(ref);
    }

    public void withProperties(EC2Properties props) {
        this.properties = props;
    }

    public void addUserData(String data) {
        this.properties.userData = data;
    }

    public static class EC2Properties {

        @JsonProperty("IamInstanceProfile")
        Ref iamInstanceProfile;
        @JsonProperty("ImageId")
        String imageId;
        @JsonProperty("KeyName")
        Ref keyName;
        @JsonProperty("InstanceType")
        String instanceType;
        @JsonProperty("SecurityGroups")
        List<Ref> securityGroups = new ArrayList<>();
        @JsonRawValue
        @JsonProperty("UserData")
        String userData;
    }
}
