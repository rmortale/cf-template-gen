/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;

/**
 *
 * @author VIDOA
 */
public class Output {

    @JsonIgnore
    public String name;

    @JsonProperty("Description")
    public String description;

    @JsonRawValue
    @JsonProperty("Value")
    public String value;

    public Output(String name, String description, String value) {
        this.name = name;
        this.description = description;
        this.value = value;
    }

}
