/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author VIDOA
 */
public class Template {

    ObjectMapper om = new ObjectMapper();

    @JsonProperty("Description")
    private final String description;

    @JsonProperty("Outputs")
    private Map<String, Output> outputs = new HashMap<>();

    @JsonProperty("Parameters")
    private Map<String, Parameter> parameters = new HashMap<>();

    @JsonProperty("Resources")
    private Map<String, Resource> resources = new HashMap<>();

    public Template(String description) {
        this.description = description;
    }

    public void addOutput(Output output) {
        this.outputs.put(output.name, output);
    }

    public void addParameter(Parameter parameter) {
        this.parameters.put(parameter.getName(), parameter);
    }

    public void addResource(Resource resource) {
        this.resources.put(resource.name, resource);
    }

    public String toJson() throws JsonProcessingException {
        return om.writerWithDefaultPrettyPrinter().writeValueAsString(this);
    }

    public void toFile(String filename) throws IOException {
        om.writerWithDefaultPrettyPrinter().writeValue(new File(filename), this);
    }
}
