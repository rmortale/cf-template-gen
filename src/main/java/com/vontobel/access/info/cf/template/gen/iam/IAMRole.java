/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen.iam;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.vontobel.access.info.cf.template.gen.Resource;

/**
 *
 * @author VIDOA
 */
public class IAMRole extends Resource {

    @JsonProperty("Type")
    public final String TYPE = "AWS::IAM::Role";

    @JsonProperty("Properties")
    Properties properties = new Properties();

    public IAMRole(String name, String policy) {
        this.name = name;
        this.properties.policy = policy;
    }

    public static class Properties {

        @JsonRawValue
        @JsonProperty("AssumeRolePolicyDocument")
        public String policy;

    }

}
