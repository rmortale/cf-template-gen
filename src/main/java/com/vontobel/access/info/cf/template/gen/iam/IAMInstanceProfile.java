/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen.iam;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vontobel.access.info.cf.template.gen.Ref;
import com.vontobel.access.info.cf.template.gen.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author VIDOA
 */
public class IAMInstanceProfile extends Resource {

    @JsonProperty("Type")
    public final String TYPE = "AWS::IAM::InstanceProfile";

    @JsonProperty("Properties")
    Properties properties = new Properties();

    public IAMInstanceProfile(String name, String path) {
        this.name = name;
        this.properties.path = path;
    }

    public void addRoleRef(Ref role) {
        this.properties.roles.add(role);
    }

    public static class Properties {

        @JsonProperty("Path")
        public String path;

        @JsonProperty("Roles")
        List<Ref> roles = new ArrayList<>();
    }

}
