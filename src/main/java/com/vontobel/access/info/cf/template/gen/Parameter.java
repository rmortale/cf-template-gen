/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Nino
 */
public class Parameter {

    public static final String EC2_KEY_PAIR_KEY_NAME = "AWS::EC2::KeyPair::KeyName";

    @JsonProperty("Description")
    private String description;

    @JsonProperty("Type")
    private String type;

    @JsonIgnore
    private String name;

    public Parameter(String name, String description, String type) {
        this.description = description;
        this.type = type;
        this.name = name;
    }

    @JsonIgnore
    public Ref getRef() {
        return new Ref(name);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
