/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vontobel.access.info.cf.template.gen;

import com.vontobel.access.info.cf.template.gen.ec2.EC2SecurityGroup;
import com.vontobel.access.info.cf.template.gen.ec2.EC2Instance;
import com.vontobel.access.info.cf.template.gen.iam.IAMInstanceProfile;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.vontobel.access.info.cf.template.gen.ec2.EC2SecurityGroup.EC2SecRule;
import static com.vontobel.access.info.cf.template.gen.ec2.EC2SecurityGroup.TCP_PROTOCOL;
import com.vontobel.access.info.cf.template.gen.iam.IAMPolicy;
import com.vontobel.access.info.cf.template.gen.iam.IAMRole;
import java.io.IOException;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author VIDOA
 */
public class JavaServerTemplateTest {

    static final String ASSUME_ROLE_POLICY_DOC = "{\n"
            + "                    \"Statement\": [\n"
            + "                        {\n"
            + "                            \"Action\": [\n"
            + "                                \"sts:AssumeRole\"\n"
            + "                            ],\n"
            + "                            \"Effect\": \"Allow\",\n"
            + "                            \"Principal\": {\n"
            + "                                \"Service\": [\n"
            + "                                    \"ec2.amazonaws.com\"\n"
            + "                                ]\n"
            + "                            }\n"
            + "                        }\n"
            + "                    ]\n"
            + "                }";

    static final String POLICY_DOCUMENT = " {\n"
            + "                    \"Statement\": [\n"
            + "                        {\n"
            + "                            \"Action\": [\n"
            + "                                \"s3:*\"\n"
            + "                            ],\n"
            + "                            \"Effect\": \"Allow\",\n"
            + "                            \"Resource\": [\n"
            + "                                \"*\"\n"
            + "                            ]\n"
            + "                        }\n"
            + "                    ]\n"
            + "                }";

    static final String USER_DATA = "{\n"
            + "                    \"Fn::Base64\": {\n"
            + "                        \"Fn::Join\": [\n"
            + "                            \"\\n\",\n"
            + "                            [\n"
            + "                                \"#!/bin/bash\",\n"
            + "                                \"yum install --enablerepo=epel -y git\",\n"
            + "                                \"pip install ansible==2.3\",\n"
            + "                                \"/usr/local/bin/ansible-pull -U https://bitbucket.org/rmortale/ansible javaserver.yml -i localinv\",\n"
            + "                                \"echo '*/10 * * * * /usr/local/bin/ansible-pull -U https://bitbucket.org/rmortale/ansible javaserver.yml -i localinv' > /etc/cron.d/ansible-pull\"\n"
            + "                            ]\n"
            + "                        ]\n"
            + "                    }\n"
            + "                }";

    static final String PUBLIC_IP_OUTPUT = " {\n"
            + "                \"Fn::GetAtt\": [\n"
            + "                    \"instance\",\n"
            + "                    \"PublicIp\"\n"
            + "                ]\n"
            + "            }";

    static final String GET_ATTR = "{ \"Fn::GetAtt\": [\"%s\",\"%s\"] }";

    Template t = new Template("a java micro services stack");

    @Test
    public void printToStdout() throws JsonProcessingException, IOException {
        Parameter p = new Parameter("KeyPair", "Name of an existing EC2 KeyPair for SSH access", Parameter.EC2_KEY_PAIR_KEY_NAME);

        EC2SecurityGroup secGroup = new EC2SecurityGroup("SecurityGroup1", "Allow SSH and TCP/3000 access");
        EC2SecRule rule1 = new EC2SecRule("0.0.0.0/0", "8080", TCP_PROTOCOL, "8080");
        EC2SecRule rule2 = new EC2SecRule("0.0.0.0/0", "22", TCP_PROTOCOL, "22");
        secGroup.addIngressRule(rule1);
        secGroup.addIngressRule(rule2);

        IAMRole role = new IAMRole("Role", ASSUME_ROLE_POLICY_DOC);

        IAMInstanceProfile profile = new IAMInstanceProfile("InstanceProfile", "/");
        profile.addRoleRef(role.getRef());

        EC2Instance host = new EC2Instance("instance", "ami-ebd02392", "t2.micro");
        host.addKeyNameRef(p.getRef());
        host.addSecurityGroupRef(secGroup.getRef());
        host.addInstanceProfileRef(profile.getRef());
        host.addUserData(USER_DATA);

        IAMPolicy policy = new IAMPolicy("Policy");
        policy.addPolicyDocument(POLICY_DOCUMENT);
        policy.addPolicyName("AllowS3");
        policy.addRoleRef(role.getRef());

        t.addParameter(p);
        t.addResource(host);
        t.addResource(secGroup);
        t.addResource(profile);
        t.addResource(role);
        t.addResource(policy);
        t.addOutput(new Output("InstancePublicIp", "Public IP of our instance.", String.format(GET_ATTR, "instance", "PublicIp")));

        String json = t.toJson();
        System.out.println(json);
        t.toFile("target/test-template.json");
        assertThat(json, containsString("ami-ebd02392"));
        assertThat(json, containsString("KeyPair"));
        assertThat(json, containsString("SecurityGroup1"));
    }

}
